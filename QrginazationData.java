import java.text.SimpleDateFormat;
import java.util.Date;

public class QrginazationData {

	private String Name;
	private String Gender;
	private String Department;
	private int Salary;
	private int Joinyear;
	private int Age;

	public QrginazationData(String name, String gender, String department, int salary, int joinyear, int age) {

		this.Name = name;
		this.Gender = gender;
		this.Department = department;
		this.Salary = salary;
		this.Joinyear = joinyear;
		this.Age = age;
	}

	public String getName() {
		return Name;
	}

	public String getGender() {
		return Gender;
	}

	public String getDepartment() {
		return Department;
	}

	public int getSalary() {
		return Salary;
	}

	public int getJoinyear() {
		return Joinyear;
	}

	public int getAge() {
		return Age;
	}

}
