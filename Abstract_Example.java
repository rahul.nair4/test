abstract class Abstract_Class_Methods {
	abstract void method1();
}

public class Abstract_Example extends Abstract_Class_Methods {
	void method1() {
		System.out.println("Overrided abstract method body");
	}

	public static void main(String[] args) {

		Abstract_Example call_method = new Abstract_Example();
		call_method.method1();

	}

}
