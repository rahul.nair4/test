
public class Polymorphism_Q4 {
	void Show() {
		System.out.println("Show() with no parameters");
	}

	void Show(int x, int y) {
		System.out.println("Show() with sum of two no:-" + (x + y));
	}

	void Show(int x, int y, int z) {
		System.out.println("Show() with sum of three no:-" + (x + y + z));
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Polymorphism_Q4 polymorphism = new Polymorphism_Q4();
		polymorphism.Show(10, 12);
		polymorphism.Show();
		polymorphism.Show(1, 2, 3);

	}

}
