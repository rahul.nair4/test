import java.io.*;

class Account implements Serializable {
	String user_name = "Rahul Nair";
	/* transient */
	transient String user_pass = "Rah123";
	transient int security_code = 123;
}

public class Serialize_Q11_Q12 {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Account account = new Account();
		System.out.println(account.user_name + "...." + account.user_pass + "...." + account.security_code);

		FileOutputStream fileOutputStream = new FileOutputStream("xyz.ser"); // FileNotFoundException
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream); // IOException
		objectOutputStream.writeObject(account);

		FileInputStream fileInputStream = new FileInputStream("xyz.ser"); // FileNotFoundException
		ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream); // IOException
		Account account1 = (Account) objectInputStream.readObject();
		System.out.println(account1.user_name + "...." + account1.user_pass + "...." + account1.security_code);

	}

}
