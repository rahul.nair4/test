
public class ThreadJoin_Q48 extends Thread {
	static Thread mainthread;

	public void run() {

		try {
			mainthread.join();
			for (int i = 1; i < 5; i++) {
				System.out.println("Thread T1 : " + i);
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws InterruptedException {
		mainthread = Thread.currentThread();
		ThreadJoin_Q48 thread = new ThreadJoin_Q48();
		// start invoke the run method
		thread.start();

		// thread.join();

		try {
			for (int i = 1; i < 5; i++) {
				System.out.println("Main Thread MT1 : " + i);
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

	}

}
