import java.util.TreeSet;

class Employee implements Comparable {
	int id;

	Employee(int id) {
		this.id = id;
	}

	public int compareTo(Object o) {
		Employee employe = (Employee) o;
		return employe.id - id;
	}

	public String toString() {
		return "" + id;
	}

}

public class Comparable_Class {

	public static void main(String[] args) {
		TreeSet idtree = new TreeSet();

		idtree.add(new Employee(10));
		idtree.add(new Employee(5));
		idtree.add(new Employee(15));
		idtree.add(new Employee(2));
		idtree.add(new Employee(1));

		System.out.println(idtree);

	}

}
