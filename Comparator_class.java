import java.util.Comparator;
import java.util.TreeSet;

abstract class Employe implements Comparator<StringBuffer> {

	/*
	 * public int compare(StringBuffer string1, StringBuffer string2) { String
	 * string_obj1 = string1.toString(); String string_obj2 = string2.toString();
	 * return string_obj1.compareTo(string_obj2); }
	 */

	public int compare(StringBuffer string1, StringBuffer string2) {
		String string_obj1 = string1.toString();
		String string_obj2 = string2.toString();
		return string_obj2.compareTo(string_obj1);
	}

}

public class Comparator_class {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeSet idtree = new TreeSet();

		idtree.add(new Employee(10));
		idtree.add(new Employee(5));
		idtree.add(new Employee(15));
		idtree.add(new Employee(2));
		idtree.add(new Employee(1));

		System.out.println(idtree);

	}

}
