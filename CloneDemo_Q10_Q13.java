import java.util.Scanner;

class UserData implements Cloneable {
	String name;
	int age;

	UserData(String name, int age) {
		this.name = name;
		this.age = age;
	}

	void ShowData() {
		System.out.println("Name:" + name + "\tAge:" + age);
	}

	protected Object clone() throws CloneNotSupportedException {

		return super.clone();
	}

}

class CloneDemo_Q10_Q13 {

	public static void main(String[] args) throws CloneNotSupportedException {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter Name and Age: ");
		String name = scanner.next();
		int age = scanner.nextInt();

		UserData userdata = new UserData(name, age);
		UserData userdata1 = (UserData) userdata.clone();
		userdata.ShowData();
		System.out.println(".......................");
		userdata1.ShowData();
	}

}
