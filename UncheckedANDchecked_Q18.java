import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Scanner;

public class UncheckedANDchecked_Q18 {

	public static void main(String[] args) throws FileNotFoundException {
		/* Remove throws FileNotFoundException */
		while (true) {
			System.out.println("----Menu----");
			System.out.println("Enter 1 for :Checked Exception");
			System.out.println("Enter 2 for :Unchecked Exception");
			System.out.println("Enter 0 for :Exit");

			Scanner scanner = new Scanner(System.in);

			int case_no = scanner.nextInt();
			switch (case_no) {
			case 1:
				FileOutputStream fileOutputStream = new FileOutputStream("xyz.ser");
				break;

			case 2:
				System.out.println(10 / 0);

			case 0:
				System.exit(1);
			}
		}

	}

}
