class Static_Reference {
	static void Show() {
		System.out.println("Static method in class A");
	}

	void Show2() {
		System.out.println("Non-Static method in class A");
	}

}

class StaticOverride_Q6 extends Static_Reference {
	static void Show() {
		System.out.println("Static method overidded in mainclass class");
	}

	void Show2() {
		System.out.println("Non-Static method in MAIN class");
	}

	public static void main(String[] args) {

		Static_Reference static_Reference = new StaticOverride_Q6();
		static_Reference.Show();
		static_Reference.Show2();

	}

}
