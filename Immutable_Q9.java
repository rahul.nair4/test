final class StringImmutable {
	private final int id;

	StringImmutable(int id) {
		this.id = id;
	}

	StringImmutable Change(int id) {
		if (this.id == id)
			return this;
		return new StringImmutable(id);
	}
}

public class Immutable_Q9 {

	public static void main(String[] args) {
		StringImmutable si = new StringImmutable(10);
		StringImmutable si1 = si.Change(10);
		StringImmutable si2 = si.Change(100);

		System.out.println(si == si1);
		System.out.print(si == si2);

	}

}
