import java.io.File;

public class ListofFD_Q21 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String path = "D:\\Eclipse-workspace\\Excercise Day1";

		File directory = new File(path);
		File[] files = directory.listFiles();
		int TotalFiles = 0, TotalDirectory = 0;

		for (File file : files) {
			if (file.isFile()) {
				TotalFiles++;
				System.out.println("File: " + file.getName());
			} else if (file.isDirectory()) {
				TotalDirectory++;
				System.out.println("Directory: " + file.getName());
			}
		}
		System.out.println("No. of Directories: " + TotalDirectory + "\nNo. of Files: " + TotalFiles);

	}

}
